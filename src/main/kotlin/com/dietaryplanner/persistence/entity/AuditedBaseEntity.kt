package com.dietaryplanner.persistence.entity

import jakarta.persistence.EntityListeners
import jakarta.persistence.MappedSuperclass
import java.time.Instant
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener

@MappedSuperclass
@EntityListeners(AuditingEntityListener::class)
abstract class AuditedBaseEntity : BaseEntity() {

    @CreatedDate
    var createdDate: Instant? = null

    @LastModifiedDate
    var lastModifiedDate: Instant? = null

}
