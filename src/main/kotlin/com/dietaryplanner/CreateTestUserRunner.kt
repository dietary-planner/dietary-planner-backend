package com.dietaryplanner

import com.dietaryplanner.persistence.entity.User
import com.dietaryplanner.persistence.entity.UserRole
import com.dietaryplanner.persistence.repository.UserRepository
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.context.annotation.Profile
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Component

@Component
@Profile("dev")
class CreateTestUserRunner(
    private val userRepository: UserRepository,
    private val passwordEncoder: PasswordEncoder
) : ApplicationRunner {
    override fun run(args: ApplicationArguments?) {
        if (userRepository.findByEmail("test@test.com") == null) {
            val user = User(
                email = "test@test.com",
                password = passwordEncoder.encode("test"),
                nickname = "test",
                roles = setOf(UserRole.USER)
            )

            userRepository.save(user)
        }
    }
}
