package com.dietaryplanner

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.data.jpa.repository.config.EnableJpaAuditing

@EnableJpaAuditing
@SpringBootApplication
class DietaryPlannerBackendApplication

fun main(args: Array<String>) {
    runApplication<DietaryPlannerBackendApplication>(*args)
}
