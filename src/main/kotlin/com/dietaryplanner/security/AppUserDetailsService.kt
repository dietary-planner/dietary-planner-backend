package com.dietaryplanner.security

import com.dietaryplanner.persistence.repository.UserRepository
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service

@Service
class AppUserDetailsService(
    private val userRepository: UserRepository
) : UserDetailsService {
    override fun loadUserByUsername(username: String): UserDetails {
        val user = userRepository.findByEmail(username) ?: throw UsernameNotFoundException("User not found")
        return AppUserDetails(
            id = user.id ?: error("User id is null"),
            email = user.email,
            password = user.password,
            authorities = user.roles.map { SimpleGrantedAuthority(it.name) }
        )
    }
}
