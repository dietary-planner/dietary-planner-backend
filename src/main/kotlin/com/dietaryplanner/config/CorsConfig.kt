package com.dietaryplanner.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.cors.CorsConfigurationSource
import org.springframework.web.cors.UrlBasedCorsConfigurationSource
import org.springframework.web.cors.CorsConfiguration as CorsConfigurationProperties

@Configuration
class CorsConfig {

    @Bean
    @ConfigurationProperties(prefix = "spring.cors")
    fun corsConfigurationProperties() = CorsConfigurationProperties()

    @Bean
    fun corsConfigurationSource(corsConfigurationProperties: CorsConfigurationProperties): CorsConfigurationSource {
        return UrlBasedCorsConfigurationSource().apply {
            registerCorsConfiguration("/**", corsConfigurationProperties)
        }
    }

}
