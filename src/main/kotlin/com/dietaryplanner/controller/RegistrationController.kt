package com.dietaryplanner.controller

import com.dietaryplanner.dto.RegistrationRequestDto
import com.dietaryplanner.service.RegistrationService
import jakarta.validation.Valid
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController

@RestController
class RegistrationController(
    private val registrationService: RegistrationService
) {

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/register")
    fun register(@RequestBody @Valid registrationRequestDto: RegistrationRequestDto) {
        registrationService.register(registrationRequestDto)
    }

}
