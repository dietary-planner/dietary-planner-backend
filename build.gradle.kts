import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("org.springframework.boot") version "3.2.0"
    id("io.spring.dependency-management") version "1.1.3"
    kotlin("jvm") version "1.9.20"
    kotlin("plugin.spring") version "1.9.20"
    kotlin("plugin.jpa") version "1.9.20"
}

group = "com.dietaryplanner"
version = "0.0.1-SNAPSHOT"

java {
    sourceCompatibility = JavaVersion.VERSION_17
}

repositories {
    mavenCentral()
}

val hibernateTypesVersion = "2.21.1"
val hibernateCommunityDialects = "6.3.2.Final"

dependencies {
    // web
    implementation("org.springframework.boot:spring-boot-starter-actuator")
    implementation("org.springframework.boot:spring-boot-starter-security")
    implementation("org.springframework.boot:spring-boot-starter-validation")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("com.google.code.gson:gson")

    // persistence
    implementation("org.springframework.boot:spring-boot-starter-data-jpa")
    // 6.3.2 contains bugfix for sqlite (https://hibernate.atlassian.net/browse/HHH-17245)
    // TODO: remove implicit version when 6.3.2 or higher will be included in spring-boot by default
    // or when database structure definition will be moved to liquibase/flyway
    implementation("org.hibernate.orm:hibernate-community-dialects:$hibernateCommunityDialects")
    implementation("org.xerial:sqlite-jdbc")
    implementation("com.vladmihalcea:hibernate-types-60:$hibernateTypesVersion")
//	implementation("org.liquibase:liquibase-core")

    // utils
    implementation("org.jetbrains.kotlin:kotlin-reflect")

    // test
    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("org.springframework.boot:spring-boot-testcontainers")
    testImplementation("org.springframework.security:spring-security-test")
    testImplementation("org.testcontainers:junit-jupiter")
}

tasks {
    withType<KotlinCompile> {
        kotlinOptions {
            freeCompilerArgs += "-Xjsr305=strict"
            jvmTarget = "17"
        }
    }

    withType<Test> {
        useJUnitPlatform()
    }

    bootBuildImage {
        builder.set("paketobuildpacks/builder-jammy-base:latest")
    }
}

allOpen {
    annotation("jakarta.persistence.Entity")
    annotation("jakarta.persistence.MappedSuperclass")
    annotation("jakarta.persistence.Embeddable")
}
