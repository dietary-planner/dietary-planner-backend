package com.dietaryplanner

import org.springframework.boot.fromApplication
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.boot.with

@TestConfiguration(proxyBeanMethods = false)
class TestDietaryPlannerBackendApplication

fun main(args: Array<String>) {
	fromApplication<DietaryPlannerBackendApplication>().with(TestDietaryPlannerBackendApplication::class).run(*args)
}
