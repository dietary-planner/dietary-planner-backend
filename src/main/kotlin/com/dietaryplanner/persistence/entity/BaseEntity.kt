package com.dietaryplanner.persistence.entity

import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id
import jakarta.persistence.MappedSuperclass

@MappedSuperclass
abstract class BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    var id: String? = null

    override fun equals(other: Any?): Boolean {
        other ?: return false
        if (this === other) return true
        if (javaClass != other.javaClass) return false

        other as BaseEntity

        return (id != null) && (id == other.id)
    }

    override fun hashCode() = 42

    override fun toString() = "${javaClass.simpleName}(id=$id)"

}
