package com.dietaryplanner.persistence.repository

import com.dietaryplanner.persistence.entity.User
import org.springframework.data.jpa.repository.JpaRepository

interface UserRepository : JpaRepository<User, String> {

    fun findByEmail(email: String): User?

    fun existsByEmail(email: String): Boolean

}
