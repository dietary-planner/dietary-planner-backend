package com.dietaryplanner.controller

import com.dietaryplanner.security.AppUserDetails
import org.springframework.context.annotation.Profile
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

// see SecurityConfig.kt
@RestController
@Profile("dev")
class SecurityTestController {

    /**
     * For secured endpoints we can use @AuthenticationPrincipal annotation to get the current user.
     */
    @GetMapping("/secured")
    fun securedEndpoint(@AuthenticationPrincipal user: AppUserDetails): Map<String, String> {
        return mapOf("message" to "Hello, World!")
    }

    @GetMapping("/unsecured")
    fun unsecuredEndpoint(): Map<String, String> {
        return mapOf("message" to "Hello, World!")
    }

}
