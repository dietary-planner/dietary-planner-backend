package com.dietaryplanner.service

import com.dietaryplanner.dto.RegistrationRequestDto
import com.dietaryplanner.persistence.entity.User
import com.dietaryplanner.persistence.repository.UserRepository
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service

@Service
class RegistrationService(
    private val userRepository: UserRepository,
    private val passwordEncoder: PasswordEncoder
) {

    fun register(registrationRequestDto: RegistrationRequestDto) {
        val encodedPassword = passwordEncoder.encode(registrationRequestDto.password)
        val user = User(
            nickname = registrationRequestDto.nickname,
            password = encodedPassword,
            email = registrationRequestDto.email
        )
        userRepository.save(user)
    }

}
