package com.dietaryplanner.dto

data class LoginSuccessResponse(
    val result: String = "SUCCESS"
)
