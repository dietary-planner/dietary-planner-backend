package com.dietaryplanner.validation

import com.dietaryplanner.persistence.repository.UserRepository
import jakarta.validation.Constraint
import jakarta.validation.ConstraintValidator
import jakarta.validation.ConstraintValidatorContext
import kotlin.reflect.KClass
import org.springframework.stereotype.Component

@Target(AnnotationTarget.FIELD, AnnotationTarget.PROPERTY_GETTER)
@Retention(AnnotationRetention.RUNTIME)
@Constraint(validatedBy = [UniqueEmailValidator::class])
annotation class UniqueEmail(
    val message: String = "email is taken",
    val groups: Array<KClass<out Any>> = [],
    val payload: Array<KClass<out Any>> = []
)

@Component
class UniqueEmailValidator(
    private val userRepository: UserRepository
) : ConstraintValidator<UniqueEmail, String?> {
    override fun isValid(email: String?, context: ConstraintValidatorContext?): Boolean {
        return if (email.isNullOrBlank()) {
            false
        } else !userRepository.existsByEmail(email)
    }
}
