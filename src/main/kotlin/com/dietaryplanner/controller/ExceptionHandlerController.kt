package com.dietaryplanner.controller

import org.springframework.http.ResponseEntity
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.bind.annotation.RestControllerAdvice

@RestController
@RestControllerAdvice
class ExceptionHandlerController {

    @ExceptionHandler
    fun handleMethodArgumentNotValidException(exception: MethodArgumentNotValidException): ResponseEntity<*> {
        // TODO: think about better error response structure
        val errors = exception.bindingResult.allErrors.map { it.defaultMessage }
        return ResponseEntity.unprocessableEntity().body(errors)
    }

}
