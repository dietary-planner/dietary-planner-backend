package com.dietaryplanner.controller

import com.dietaryplanner.dto.LoginSuccessResponse
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class LoginController {

    /**
     * Basic auth doesn't suppose any login actions, so we just return a success response.
     */
    @PostMapping("/login")
    fun login(): LoginSuccessResponse {
        return LoginSuccessResponse()
    }

}
