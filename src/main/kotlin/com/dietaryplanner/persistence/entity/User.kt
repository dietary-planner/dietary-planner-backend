package com.dietaryplanner.persistence.entity

import com.vladmihalcea.hibernate.type.json.JsonType
import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.Table
import jakarta.persistence.UniqueConstraint
import org.hibernate.annotations.Type

@Entity
@Table(
    name = "users",
    // TODO: define all constraints in flyway/liquibase migrations
    uniqueConstraints = [UniqueConstraint(columnNames = ["email"])]
)
class User(

    val nickname: String,
    val email: String,
    var password: String,

    @Type(JsonType::class)
    @Column(columnDefinition = "json")
    val roles: Set<UserRole> = setOf(UserRole.USER),
) : AuditedBaseEntity()


enum class UserRole {
    USER,
    ADMIN;
}
