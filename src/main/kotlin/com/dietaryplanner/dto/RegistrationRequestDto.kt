package com.dietaryplanner.dto

import com.dietaryplanner.validation.UniqueEmail
import jakarta.validation.constraints.Email
import jakarta.validation.constraints.NotBlank
import jakarta.validation.constraints.NotNull

data class RegistrationRequestDto(
    @get:NotNull
    @get:NotBlank
    val nickname: String,

    @get:NotNull
    @get:NotBlank
    val password: String,

    @get:NotNull
    @get:NotBlank
    @get:Email
    @get:UniqueEmail
    val email: String
)
