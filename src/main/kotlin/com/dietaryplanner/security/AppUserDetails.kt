package com.dietaryplanner.security

import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.User as SpringSecurityUser

class AppUserDetails(
    val id: String,
    email: String,
    password: String,
    authorities: List<GrantedAuthority>
) : SpringSecurityUser(email, password, authorities)
